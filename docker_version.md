# Docker Version Mapping

## 2.9.0

Released on 23<sup>th</sup> December 2024

| Services         | Docker Image            | Image ID         | Remark |
| ---------------- | ----------------------- | ---------------- | ------ |
| **ekyc_service** | **fastapi_ekyc:1.6.0**  | **323ff7db6a36** |        |
| **ai_server**    | **ai:1.7.0**            | **d4ccfda4c4c2** |        |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |        |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |        |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |        |

## 2.8.0

Released on 14<sup>th</sup> November 2024

| Services         | Docker Image            | Image ID         | Remark |
| ---------------- | ----------------------- | ---------------- | ------ |
| **ekyc_service** | **fastapi_ekyc:1.5.0**  | **669306749bb0** |        |
| **ai_server**    | **ai:1.6.1**            | **087defa9fcaf** |        |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |        |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |        |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |        |

## 2.7.7

Released on 14<sup>th</sup> November 2024

| Services         | Docker Image            | Image ID         | Remark |
| ---------------- | ----------------------- | ---------------- | ------ |
| **ekyc_service** | **fastapi_ekyc:1.3.3**  | **7aa75293b173** |        |
| ai_server        | ai:1.5.6                | 4cbcaff2f324     |        |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |        |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |        |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |        |

## 2.7.6

Released on 28<sup>th</sup> October 2024

| Services      | Docker Image            | Image ID         | Remark                                 |
| ------------- | ----------------------- | ---------------- | -------------------------------------- |
| ekyc_service  | fastapi_ekyc:1.3.0      | 097b7aed4a80     |                                        |
| **ai_server** | **ai:1.5.6**            | **4cbcaff2f324** | **liveness: infer_model-10_ccnn.hdf5** |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                        |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |                                        |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |                                        |

## 2.7.5

Released on 10<sup>th</sup> October 2024

| Services      | Docker Image            | Image ID         | Remark                                 |
| ------------- | ----------------------- | ---------------- | -------------------------------------- |
| ekyc_service  | fastapi_ekyc:1.3.0      | 097b7aed4a80     |                                        |
| **ai_server** | **ai:1.5.5**            | **54b7b2751ad9** | **liveness: infer_model-06_ccnn.hdf5** |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                        |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |                                        |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |                                        |

## 2.7.4

Released on 27<sup>th</sup> September 2024

| Services      | Docker Image            | Image ID         | Remark                                 |
| ------------- | ----------------------- | ---------------- | -------------------------------------- |
| ekyc_service  | fastapi_ekyc:1.3.0      | 097b7aed4a80     |                                        |
| **ai_server** | **ai:1.5.4**            | **61f5163bad0b** | **liveness: infer_model-02_ccnn.hdf5** |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                        |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |                                        |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |                                        |

## 2.7.3

Released on 5<sup>th</sup> September 2024

| Services      | Docker Image            | Image ID         | Remark                                                                       |
| ------------- | ----------------------- | ---------------- | ---------------------------------------------------------------------------- |
| ekyc_service  | fastapi_ekyc:1.3.0      | 097b7aed4a80     |                                                                              |
| **ai_server** | **ai:1.5.3**            | **7909d074bf19** | **idrecapture: parallel_Ench14_v2_20240901-1721_Ench14_v2_20240830-0943.h5** |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                                              |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |                                                                              |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |                                                                              |

## 2.7.2

Released on 9<sup>th</sup> August 2024

| Services      | Docker Image            | Image ID         | Remark                                                                       |
| ------------- | ----------------------- | ---------------- | ---------------------------------------------------------------------------- |
| ekyc_service  | fastapi_ekyc:1.3.0      | 097b7aed4a80     |                                                                              |
| **ai_server** | **ai:1.5.2**            | **e406d44a91b5** | **idrecapture: parallel_Ench13_v3_20240807-1055_Ench13_v3_20240806-0941.h5** |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                                              |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |                                                                              |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |                                                                              |

## 2.7.1

Released on 7<sup>th</sup> August 2024

| Services      | Docker Image            | Image ID         | Remark                                 |
| ------------- | ----------------------- | ---------------- | -------------------------------------- |
| ekyc_service  | fastapi_ekyc:1.3.0      | 097b7aed4a80     |                                        |
| **ai_server** | **ai:1.5.1**            | **8d37ab72a3b1** | **liveness: infer_model-09_ccnn.hdf5** |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                        |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |                                        |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |                                        |

## 2.7.0

Released on 2<sup>nd</sup> August 2024

| Services         | Docker Image            | Image ID         | Remark                                                                                                                                                                                                                                          |
| ---------------- | ----------------------- | ---------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **ekyc_service** | **fastapi_ekyc:1.3.0**  | **097b7aed4a80** |                                                                                                                                                                                                                                                 |
| **ai_server**    | **ai:1.5.0**            | **09108b01ac4f** | **liveness: infer_model-04_ccnn.hdf5**<br/><br>**idphysicaltamper: Exp_Ench6_Colored_Ghost_Tampered_20240711-1534_checkpoint_21.h5**<br/><br>**idrecapture: parallel_Ench12_Backup_v2_no2_20240722-1316_Ench12_Backup_v2_no2_20240722-1312.h5** |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                                                                                                                                                                                                                 |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |                                                                                                                                                                                                                                                 |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |                                                                                                                                                                                                                                                 |

## 2.6.8

Released on 17<sup>th</sup> June 2024

| Services         | Docker Image            | Image ID         | Remark |
| ---------------- | ----------------------- | ---------------- | ------ |
| **ekyc_service** | **fastapi_ekyc:1.2.4**  | **a97b70b6d7c2** |        |
| **ai_server**    | **ai:1.4.7**            | **350c235db1d9** |        |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |        |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |        |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |        |

## 2.6.7

Released on 10<sup>th</sup> May 2024

| Services         | Docker Image            | Image ID         | Remark                                                                                                                                                                                                                                                                        |
| ---------------- | ----------------------- | ---------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **ekyc_service** | **fastapi_ekyc:1.2.3**  | **8d7eac93e998** |                                                                                                                                                                                                                                                                               |
| **ai_server**    | **ai:1.4.6**            | **7de66d0a2db7** | **idphysicaltamper: TamperTrain_20240429-1750_checkpoint_26.h5**<br/><br>**idrecapture: parallel_Test_Square_Backup_Model_20240429-1445_square_Test_Square_Backup_Model_20240426-1812_square.h5**<br/><br>**landmark_back: yolox.YOLOXS-none_20240506-1725_ckpt_epoch_28.h5** |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                                                                                                                                                                                                                                               |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |                                                                                                                                                                                                                                                                               |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |                                                                                                                                                                                                                                                                               |

## 2.6.6

Released on 3<sup>rd</sup> May 2024

| Services         | Docker Image            | Image ID         | Remark                                                   |
| ---------------- | ----------------------- | ---------------- | -------------------------------------------------------- |
| **ekyc_service** | **fastapi_ekyc:1.2.2**  | **13ddd9e7e03f** |                                                          |
| **ai_server**    | **ai:1.4.5_automl**     | **1d4ce89c8f98** | **idrecapture: parallel_20240326-0111_20240325-1540.h5** |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                          |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |                                                          |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |                                                          |

## 2.6.5

Released on 22<sup>nd</sup> March 2024

| Services      | Docker Image            | Image ID         | Remark                                                                                                                                  |
| ------------- | ----------------------- | ---------------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| ekyc_service  | fastapi_ekyc:1.2.1      | cb3141476a29     |                                                                                                                                         |
| **ai_server** | **ai:1.4.4**            | **6b76779b41ed** | **idphysicaltamper: yolox.YOLOXS-None_20240315-1756_ckpt_epoch_16.h5**<br/><br>**idrecapture: parallel_20240315-0018_20240314-1616.h5** |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                                                                                                         |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |                                                                                                                                         |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |                                                                                                                                         |

## 2.6.4

Released on 26<sup>th</sup> February 2024

| Services      | Docker Image            | Image ID         | Remark                                                                  |
| ------------- | ----------------------- | ---------------- | ----------------------------------------------------------------------- |
| ekyc_service  | fastapi_ekyc:1.2.1      | cb3141476a29     |                                                                         |
| **ai_server** | **ai:1.4.3**            | **d8e4bf4de1a1** | **idphysicaltamper: yolox.YOLOXS-None_20240222-2028_ckpt_epoch_11.h5s** |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                                         |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |                                                                         |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |                                                                         |

## 2.6.3

Released on 24<sup>th</sup> January 2024

| Services         | Docker Image            | Image ID         | Remark |
| ---------------- | ----------------------- | ---------------- | ------ |
| **ekyc_service** | **fastapi_ekyc:1.2.1**  | **cb3141476a29** |        |
| ai_server        | ai:1.4.2                | 9fbf164176e7     |        |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |        |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |        |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |        |

## 2.6.2

Released on 17<sup>th</sup> November 2023

| Services      | Docker Image            | Image ID         | Remark                                     |
| ------------- | ----------------------- | ---------------- | ------------------------------------------ |
| ekyc_service  | fastapi_ekyc:1.2.0      | 6de80b2c7d4e     |                                            |
| **ai_server** | **ai:1.4.2**            | **9fbf164176e7** | **idrecapture: parallel-20231106-1354.h5** |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                            |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |                                            |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |                                            |

## 2.6.1

Released on 20<sup>th</sup> September 2023

| Services      | Docker Image            | Image ID         | Remark |
| ------------- | ----------------------- | ---------------- | ------ |
| ekyc_service  | fastapi_ekyc:1.2.0      | 6de80b2c7d4e     |        |
| **ai_server** | **ai:1.4.1**            | **0b1b15a595a6** |        |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |        |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |        |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |        |

## 2.6.0

Released on 31<sup>st</sup> July 2023

| Services         | Docker Image            | Image ID         | Remark                                                                                                                                                                                                  |
| ---------------- | ----------------------- | ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **ekyc_service** | **fastapi_ekyc:1.2.0**  | **6de80b2c7d4e** |                                                                                                                                                                                                         |
| **ai_server**    | **ai:1.4.0**            | **862c95059802** | **<br>liveness: ckpt 17 (log_CCNN_all_xtra_iso_liveattk_r7)**<br/><br>**idphysicaltamper: YOLOXS-None_20230731-1112-epoch_11.h5**<br/> <br>**mykadfront: yolox.YOLOXS-None_20230720-1707-epoch35**<br/> |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                                                                                                                                                                         |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |                                                                                                                                                                                                         |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |                                                                                                                                                                                                         |

## 2.5.0

Released on 19<sup>th</sup> June 2023

| Services         | Docker Image            | Image ID         | Remark                                          |
| ---------------- | ----------------------- | ---------------- | ----------------------------------------------- |
| **ekyc_service** | **fastapi_ekyc:1.1.3**  | **e1c1344daf71** |                                                 |
| **ai_server**    | **ai:1.3.0**            | **16be9c1a316f** | **idphysicaltamper: resnet50-20230618-2123.h5** |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                 |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |                                                 |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |                                                 |

## 2.4.1

Released on 9<sup>th</sup> June 2023

| Services         | Docker Image            | Image ID         | Remark                                                 |
| ---------------- | ----------------------- | ---------------- | ------------------------------------------------------ |
| **ekyc_service** | **fastapi_ekyc:1.1.2**  | **f2e0b647e7b2** |                                                        |
| **ai_server**    | **ai:1.2.1**            | **cbb9c3cf3618** | **mykadfront: yolox.YOLOXS-None_20230608-2207-epoch8** |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                        |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |                                                        |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |                                                        |

## 2.4.0

Released on 12<sup>st</sup> May 2023

| Services         | Docker Image            | Image ID         | Remark                                                                                                                                                                               |
| ---------------- | ----------------------- | ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **ekyc_service** | **fastapi_ekyc:1.1.1**  | **abd2ad35e0a6** |                                                                                                                                                                                      |
| **ai_server**    | **ai:1.2.0**            | **5942cec364d6** | **<br>liveness: ckpt 25 (log_domain_invariant_nosiamese_v3)**<br/><br>**idrecapture: parallel-20230419-1840.h5**<br/> <br>**mykadback: yolox.YOLOXS-None_20230425-1547-epoch5**<br/> |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                                                                                                                                                      |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |                                                                                                                                                                                      |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |                                                                                                                                                                                      |

## 2.3.0

Released on 14<sup>st</sup> April 2023

| Services         | Docker Image            | Image ID         | Remark                                                                                                                                                           |
| ---------------- | ----------------------- | ---------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **ekyc_service** | **fastapi_ekyc:1.1.0**  | **253541e2725c** |                                                                                                                                                                  |
| **ai_server**    | **ai:1.1.0**            | **fc36d87fd745** | **<br>liveness: ckpt 19 (iso_liveattk_r4_v10)**<br/><br>**idrecapture: parallel-20230414-1016.h5**<br/> <br>**mykadfront: yolox.YOLOXS-None_20230414-1048**<br/> |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                                                                                                                                  |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |                                                                                                                                                                  |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |                                                                                                                                                                  |

## 2.2.5

Released on 31<sup>st</sup> March 2023

| Services      | Docker Image            | Image ID         | Remark                                     |
| ------------- | ----------------------- | ---------------- | ------------------------------------------ |
| ekyc_service  | fastapi_ekyc:1.0.3      | 546131f4af21     |                                            |
| **ai_server** | **ai:1.0.5**            | **8fbe1fc314d2** | **liveness: ckpt 47 (iso_liveattk_r4_v8)** |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                            |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |                                            |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |                                            |

## 2.2.4

Released on 17<sup>th</sup> March 2023

| Services         | Docker Image            | Image ID         | Remark                                     |
| ---------------- | ----------------------- | ---------------- | ------------------------------------------ |
| **ekyc_service** | **fastapi_ekyc:1.0.3**  | **546131f4af21** |                                            |
| **ai_server**    | **ai:1.0.4**            | **e88ee517a8ef** | **liveness: ckpt 50 (iso_liveattk_r4_v7)** |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                            |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |                                            |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |                                            |

## 2.2.3

Released on 4<sup>th</sup> March 2023

| Services         | Docker Image            | Image ID         | Remark                                     |
| ---------------- | ----------------------- | ---------------- | ------------------------------------------ |
| **ekyc_service** | **fastapi_ekyc:1.0.2**  | **4e5b9803d6d3** |                                            |
| **ai_server**    | **ai:1.0.3**            | **c0bccda8100a** | **liveness: ckpt 46 (iso_liveattk_r4_v6)** |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                            |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |                                            |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |                                            |

## 2.2.2

Released on 9<sup>th</sup> January 2023

| Services      | Docker Image            | Image ID         | Remark                                                                                   |
| ------------- | ----------------------- | ---------------- | ---------------------------------------------------------------------------------------- |
| ekyc_service  | fastapi_ekyc:1.0.1      | 0c7978338086     |                                                                                          |
| **ai_server** | **ai:1.0.2**            | **ceaf03de8a32** | **liveness: ckpt 22 (iso_liveattk_r4)**<br>**idrecapture: parallel-221121-1410.h5**<br/> |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                                                                          |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |                                                                                          |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |                                                                                          |

## 2.2.1

Released on 29<sup>th</sup> November 2022

| Services      | Docker Image            | Image ID         | Remark                                 |
| ------------- | ----------------------- | ---------------- | -------------------------------------- |
| ekyc_service  | fastapi_ekyc:1.0.1      | 0c7978338086     |                                        |
| **ai_server** | **ai:1.0.1**            | **1108fef5996d** | **liveness: ckpt 5 (iso_liveattk_r2)** |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |                                        |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |                                        |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |                                        |

## 2.2.0

Released on 11<sup>th</sup> November 2022

| Services      | Docker Image            | Image ID         |
| ------------- | ----------------------- | ---------------- |
| ekyc_service  | fastapi_ekyc:1.0.1      | 0c7978338086     |
| **ai_server** | **ai:1.0.0**            | **30ebcaebd34a** |
| fr_zmq_server | fr_zmq_engine:1.1.1     | 2cc912d599e1     |
| fr_server     | fr_engine:1.0.0         | 9f6a85d8782e     |
| rabbitmq      | rabbitmq:wiseai-version | 73d81411a25c     |

## 2.1.0

Released on 28<sup>th</sup> October 2022

| Services         | Docker Image            | Image ID         |
| ---------------- | ----------------------- | ---------------- |
| **ekyc_service** | **fastapi_ekyc:1.0.1**  | **0c7978338086** |
| all_tf_server    | all_tf:1.0.0            | a34edad49e29     |
| ocr_server       | ocr:2.0.0               | 902f396fd575     |
| liveness_server  | liveness:2.0.0          | 44cdcf11b3b1     |
| fr_zmq_server    | fr_zmq_engine:1.1.1     | 2cc912d599e1     |
| fr_server        | fr_engine:1.0.0         | 9f6a85d8782e     |
| rabbitmq         | rabbitmq:wiseai-version | 73d81411a25c     |

## 2.0.0

Released on 11<sup>th</sup> October 2022

| Services            | Docker Image            | Image ID         |
| ------------------- | ----------------------- | ---------------- |
| **ekyc_service**    | **fastapi_ekyc:1.0.0**  | **6c4354b27fdd** |
| **all_tf_server**   | **all_tf:1.0.0**        | **a34edad49e29** |
| **ocr_server**      | **ocr:2.0.0**           | **902f396fd575** |
| **liveness_server** | **liveness:2.0.0**      | **44cdcf11b3b1** |
| **fr_zmq_server**   | **fr_zmq_engine:1.1.1** | **2cc912d599e1** |
| fr_server           | fr_engine:1.0.0         | 9f6a85d8782e     |
| rabbitmq            | rabbitmq:wiseai-version | 73d81411a25c     |

## 1.1.4

Released on 1<sup>st</sup> August, 2022

| Services                | Docker Image            | Image ID         |
| ----------------------- | ----------------------- | ---------------- |
| ekyc_service            | flask_client:1.4        | f32bf650296a     |
| segmentation_server     | segmentation:1.1        | c4a892f1c3ef     |
| mykadfront_server       | mykadfront:1.1          | 8280f4fbd7c2     |
| mykadback_server        | mykadback:1.1           | 8280f4fbd7c2     |
| ocr_server              | ocr:1.1                 | 69bf6576b6aa     |
| idcapture_server        | idcapture:1.3           | dcad7cec2a59     |
| idphysicaltamper_server | idphysicaltamper:1.0    | 5d121245d365     |
| **liveness_server**     | **liveness:1.4**        | **ffa3d70a64cf** |
| fr_zmq_server           | fr_zmq_engine:1.1.0     | 2df71a9a29b9     |
| fr_server               | fr_engine:1.0.0         | 4d951d03e616     |
| rabbitmq                | rabbitmq:wiseai-version | 73d81411a25c     |

## 1.1.3

Released on 15<sup>th</sup> July, 2022

| Services                | Docker Image            | Image ID         |
| ----------------------- | ----------------------- | ---------------- |
| **ekyc_service**        | **flask_client:1.4**    | **f32bf650296a** |
| segmentation_server     | segmentation:1.1        | c4a892f1c3ef     |
| mykadfront_server       | mykadfront:1.1          | 8280f4fbd7c2     |
| mykadback_server        | mykadback:1.1           | 8280f4fbd7c2     |
| ocr_server              | ocr:1.1                 | 69bf6576b6aa     |
| **idcapture_server**    | **idcapture:1.3**       | **dcad7cec2a59** |
| idphysicaltamper_server | idphysicaltamper:1.0    | 5d121245d365     |
| liveness_server         | liveness:1.3            | 60ec42ff01f1     |
| fr_zmq_server           | fr_zmq_engine:1.1.0     | 2df71a9a29b9     |
| fr_server               | fr_engine:1.0.0         | 4d951d03e616     |
| rabbitmq                | rabbitmq:wiseai-version | 73d81411a25c     |

## 1.1.2

Released on 14<sup>th</sup> June, 2022

| Services                | Docker Image            | Image ID         |
| ----------------------- | ----------------------- | ---------------- |
| **ekyc_service**        | **flask_client:1.3**    | **699b1298a02f** |
| segmentation_server     | segmentation:1.1        | c4a892f1c3ef     |
| mykadfront_server       | mykadfront:1.1          | 8280f4fbd7c2     |
| mykadback_server        | mykadback:1.1           | 8280f4fbd7c2     |
| ocr_server              | ocr:1.1                 | 69bf6576b6aa     |
| idcapture_server        | idcapture:1.2           | 2237faabdacd     |
| idphysicaltamper_server | idphysicaltamper:1.0    | 5d121245d365     |
| **liveness_server**     | **liveness:1.3**        | **60ec42ff01f1** |
| **fr_zmq_server**       | **fr_zmq_engine:1.1.0** | **2df71a9a29b9** |
| fr_server               | fr_engine:1.0.0         | 4d951d03e616     |
| rabbitmq                | rabbitmq:wiseai-version | 73d81411a25c     |

## 1.1.1

Released on 22<sup>th</sup> April, 2022

| Services                | Docker Image            | Image ID     |
| ----------------------- | ----------------------- | ------------ |
| ekyc_service            | flask_client:1.2        | bb6ab4cc957a |
| segmentation_server     | segmentation:1.1        | c4a892f1c3ef |
| mykadfront_server       | mykadfront:1.1          | 8280f4fbd7c2 |
| mykadback_server        | mykadback:1.1           | 8280f4fbd7c2 |
| ocr_server              | ocr:1.1                 | 69bf6576b6aa |
| idcapture_server        | idcapture:1.2           | 2237faabdacd |
| idphysicaltamper_server | idphysicaltamper:1.0    | 5d121245d365 |
| liveness_server         | liveness:1.2            | 5ccdc937a0ce |
| fr_zmq_server           | fr_zmq_engine:1.0.0     | a2955b45925d |
| fr_server               | fr_engine:1.0.0         | 4d951d03e616 |
| rabbitmq                | rabbitmq:wiseai-version | 73d81411a25c |
