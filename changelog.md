# Major Change log for AI Engine

## 2.9.0
---
Released on 23<sup>th</sup> December 2024

### Enhanced
* Improved IDRecapture detection to reduce false acceptance on unseen data

## 2.8.0
---
Released on 14<sup>th</sup> November 2024

### Enhanced
* Added identity document endpoint that accept mykad, mytentera and mypr
* Modified liveness API to return face feature
* Fixed bug for mykad_back ocr logic

## 2.7.7
---
Released on 14<sup>th</sup> November 2024

### Enhanced
* Added route for face feature extraction
* Modified the logic of card rotation
* Modified the logic of handling image input

## 2.7.6
---
Released on 28<sup>th</sup> October 2024

### Enhanced
* Improved Liveness detection to reduce false acceptance on unseen data

## 2.7.5
---
Released on 10<sup>th</sup> October 2024

### Enhanced
* Improved Liveness detection to reduce false acceptance on unseen data

## 2.7.4
---
Released on 27<sup>th</sup> September 2024

### Enhanced
* Improved Liveness detection to reduce false acceptance on unseen data

## 2.7.3
---
Released on 5<sup>th</sup> September 2024

### Enhanced
* Improved ID recapture model to reduce false acceptance on unseen data

## 2.7.2
---
Released on 9<sup>th</sup> August 2024

### Enhanced
* Improved ID recapture model to reduce false acceptance on unseen data

## 2.7.1
---
Released on 7<sup>th</sup> August 2024

### Enhanced
* Improved Liveness detection to reduce false acceptance on unseen data

## 2.7.0
---
Released on 2<sup>nd</sup> August 2024

### Enhanced
* Improved Liveness detection to reduce false acceptance on unseen data
* Improved ID recapture model to reduce false acceptance on unseen data
* Improved ID physical tampering model to reduce false acceptance on unseen data
* Improved IC number validation logic

## 2.6.8
---
Released on 17<sup>th</sup> June 2024

### Enhanced
* Enabled liveness threshold parameter
* Modified the logic of OCR post processing

## 2.6.7
---
Released on 10<sup>th</sup> May 2024

### Enhanced
* Improved ID recapture model & preprocess logic to reduce false rejection and false acceptance on unseen data
* Improved ID physical tampering model to reduce false rejection on unseen data
* Improved MyKad Back landmark result for occasiional missing signature landmark

## 2.6.6
---
Released on 3<sup>rd</sup> May 2024

### Enhanced
* Improved ID recapture model to reduce false rejection on unseen data

### Changed
* Modified the logic of image frame extraction when the input video's length is equal to 3 frames

## 2.6.5
---
Released on 22<sup>nd</sup> March 2024

### Enhanced
* Improved ID recapture model to reduce false rejection and false acceptance on unseen data
* Improved ID physical tampering model to reduce false acceptance on unseen data

## 2.6.4
---
Released on 26<sup>th</sup> Feb 2024

### Enhanced
* Improved ID physical tampering model to reduce false acceptance on unseen data.

## 2.6.3
---
Released on 24<sup>th</sup> Jan 2024

### Enhanced
* Improved landmark result when Mykad front's address is detected glare.

## 2.6.2
---
Released on 17<sup>th</sup> Nov 2023

### Added
* Improved ID recapture model, reduce false rejection and false positive on unseen data

## 2.6.1
---
Released on 20<sup>th</sup> September 2023

### Enhanced
* Improved OCR result for Mykad back's ID no, mitigate false OCR result when there is other character nearby Mykad back's ID no.
### Changed
* Changed the logic when Mykad front's address is detected as bad quality so that it would return the OCR result even though it's bad quality.

## 2.6.0
---
Released on 31<sup>st</sup> July 2023

### Added
* New backbone and deployment code for ID physical tampering model, reduce false rejection and false positive on unseen data

### Enhanced
* Improved Liveness detection, reduce false rejection and false positive on unseen data, which is also the release version for iBeta testing
* Improved MyKad Front quality checking result, reduce false acceptance when motion blur and reduce false rejection when ocr is visible

## 2.5.0
---
Released on 19<sup>th</sup> June 2023

### Enhanced
* Improved ID physical tampering model, reduce false rejection and false positive on unseen data

## 2.4.1
---
Released on 9<sup>th</sup> June 2023

### Enhanced
* Improved MyKad Front quality checking result, reduce false acceptance when landmark is occluded or glare

## 2.4.0
---
Released on 12<sup>st</sup> May 2023

### Enhanced
* Improved Liveness detection, reduce false rejection and false positive on unseen data
* Improved ID recapture model, reduce false rejection and false positive on unseen data
* Improved MyKad Front landmark result for occasiional missing name landmark
* Improved MyKad Back quality checking result, reduce false rejection when OCR is visible
* Improved mykadfront_cardLogo ocr result for mytentera flow by landmark info

## 2.3.0
---
Released on 14<sup>st</sup> April 2023

### Enhanced
* Improved Liveness detection, reduce false rejection and false positive on unseen data
* Improved ID recapture model, reduce false rejection and false positive on unseen data
* Improved MyKad Front quality checking result, reduce false rejection when OCR is visible

## 2.2.5
---
Released on 31<sup>st</sup> March 2023

### Enhanced
* Improved Liveness detection, reduce false rejection and false positive on unseen data

## 2.2.4
---
Released on 17<sup>th</sup> March 2023

### Enhanced
* Improved Liveness detection, reduce false rejection and false positive on unseen data

## 2.2.3
---
Released on 4<sup>th</sup> March 2023

### Enhanced
* Improved Liveness detection, reduce false rejection and false positive on unseen data

## 2.2.2
---
Released on 9<sup>th</sup> January 2023

### Enhanced
* Improved Liveness detection, reduce false rejection and false positive on unseen data

### Added
* Added a new flag to switch between idcapture original and parallel model

## 2.2.1
---
Released on 29<sup>th</sup> November 2022

### Enhanced
* Improved Liveness detection, reduce false rejection and false positive on unseen data

## 2.2.0
---
Released on 11<sup>th</sup> November 2022

### Enhanced
* Improved Liveness detection, reduce false rejection and false positive on unseen data
* Combined all python-based AI servers into one single docker image, reduced installation files size from 26GB to 11GB
* Optimized gpu memory usage: from 10GB to 7.5GB
* Optimized RAM usage: from 18GB to 13GB

## 2.1.0
---
Released on 28<sup>th</sup> October 2022

### Added
* Added a new ghost face image campare module for idfraud

## 2.0.0
---
Released on 11<sup>th</sup> October 2022

### Enhanced
* Improved Liveness detection, reduce false rejection and false positive on unseen data
* Optimized efficiency of an ekyc request by shared_memory and FastAPI, below are details of the average latency

    * mykad_front: from 2.5s to 1.2s
    * mykad_back: from 2.3s to 0.8s
    * face recognition + face liveness: from 0.55 to 0.5s
  
* Optimized gpu memory usage: from 14GB to 10GB
* Optimized RAM usage: from 28GB to less than 20GB

### Added
* Added a new route to perform face compare given two images

## 1.1.5
---
Released on 15<sup>th</sup> August 2022

### Enhanced

* Enhanced ID recapture detection by training model with old and new dataset for mykad front only. 

### Changed

* Removed idfraud's mykad back module
* Removed color conversion in flask

## 1.1.4
---
Released on 1<sup>st</sup> August 2022

### Enhanced

* Improved Liveness detection by new backbone which will further reduce false rejection and false positive on unseen data

## 1.1.3
---
Released on 15<sup>th</sup> July 2022

### Enhanced

* Enhanced ID recapture detection by training model with newly collected printed ID dataset. Reduced Bona Fide presentation classification error rate (BPCER).

## 1.1.2
---
Released on 14<sup>th</sup> June 2022

### Enhanced

* Improved Liveness detection by reducing false rejection and false positive
* Enhanced face detector for low quality image

### Changed

* Solved "Internal Server Error" bug for liveness when an exceptional case happened

## 1.1.1
---
Released on 22<sup>nd</sup> April, 2022

### Enhanced

* Improved Liveness detection by reducing false rejection

## 1.1.0
---
Released on 14<sup>th</sup> April, 2022

### Added

* Added a new ID Fraud modules: physical tampering detection

### Enhanced

* Improved ID recapture model
  * print
  * replay

### Changed

* Removed mandatory photo copy detection (Replaced by ID Fraud modules)



## 1.0.3
---
Released on 14<sup>th</sup> February, 2022

### Changed

* Solved FR Server high load latency issue via concurrent method



## 1.0.2
---
Released on 22<sup>nd</sup> December, 2021

### Added

* Support for MyPR and MyTentera ID verification

### Enhanced

* Improved glare detection speed via C++
* Improved text detection speed
* Improved Liveness model by adding more replay data

### Changed

* Modified and added more generalized field for landmark and OCR to support for MyPR and MyTentera

* Added new endpoints to support for:

  * mytentera front & back
  * mypr front & back

  

## 1.0.1
---
Released on 11<sup>st</sup> Octorber, 2021

### Enhanced

* Improved Liveness model in reducing False Negatives



## 1.0.0
---
Released on 29<sup>th</sup> July, 2021

### Enhanced

* Optimized FR engine speed from 2s to 300ms per inference
* Enabled multiple OCR servers to cater for high load support
* Reduced GPU memory usage for landmark detection models
* Optimized Text Detection module postprocessing

### Added

* Added flag for CPU inference
* Merged Face Recognition and Face Liveness (face_all) as a standalone endpoint

### Changed

* Fixed OCR server timeout response

* Added documentation for face_all

* Fixed bug for Liveness module when the input face is occluded

  

## 0.1.0
---
Internal Released on 19<sup>th</sup> April , 2021

### Added

* First version of the WISE AI On Premise AI Engine, containing:
  * 4 APIs for returning result of
    * Mykad Identity Document (ID) Verification and Optical Character Recognition (OCR)
      * Mykad Front
      * Mykad Back
    * Face Liveness Detection
    * Face Recognition
    * Health Check
  * 8 AI Models Servers
    * ID Segmentation
    * Mykad Front Landmark Detection
    * Mykad Back Landmark Detection
    * ID Recapture Detection
    * OCR
    * Face Detection
    * Face Recognition (FR)
    * Liveness Detection
  * Glare and blur detection for ID text fields (for eg. name, NRIC, address) notify possible drop of OCR accuracy. 
  * Swagger API documentation